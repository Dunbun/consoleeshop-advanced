﻿namespace ConsoleEShop_Low
{
    public interface IApplication
    { 
        ISecurityService SecurityService { get; }
    }
}
