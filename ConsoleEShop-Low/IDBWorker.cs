﻿using System.Collections.Generic;

namespace ConsoleEShop_Low
{
    public interface IDBWorker
    {
        void CreateGoods(Goods goods);
        void UpdateOrderStatus(int orderId, OrderStatus status);
        void ChangeGoodsInfo(Goods goods);
        void ChangeUserEmail(int userId, string newEmail);
        void ChangeUserName(int userId, string newName);
        void ChangeUserPassword(int userId, string oldPassword, string newPassword);
        void CreateNewOrder(string goodsName, int userId);
        Goods GetGoodsByName(string goodName);
        List<Goods> GetGoodsList();
        List<Order> GetOrdersHistory();
        OrderStatus GetOrderStatus(int orderId);
        RegisteredUserInfo GetUserInfo(int Id);

        bool CreateAccount(string email, string password, string name, UserRoles role);
        RegisteredUserInfo FindAccount(string email, string password);
    }
}