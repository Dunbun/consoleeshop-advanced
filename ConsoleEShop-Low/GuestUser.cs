﻿using System;

namespace ConsoleEShop_Low
{
    public class GuestUser : User
    {
        public GuestUser(IDBWorker dBWorker) : base(dBWorker)
        {
        }

        public override void OpenMenu()
        {
            string input;

            do
            {
                Console.WriteLine("Menu for guest user:");
                Console.WriteLine("1 - Create account");
                Console.WriteLine("2 - Find item by name");
                Console.WriteLine("3 - Get list of goods");
                Console.WriteLine("4 - Exit");

                input = Console.ReadLine();

                if (!int.TryParse(input, out var cmd))
                {
                    continue;
                }

                switch (cmd)
                {
                    case 1:
                        CreateAccount();
                        break;

                    case 2:
                        FindItemByName();
                        break;

                    case 3:
                        GetItemsList();
                        break;

                    case 4:
                        return;
                }
            }
            while (!string.IsNullOrWhiteSpace(input));
        }

        private void GetItemsList()
        {
            foreach (var goods in base.GetGoodsList())
                Console.WriteLine($"{goods.Name}, {goods.Category}, {goods.Description}");

            Console.WriteLine("------------------------------------------------------");
        }

        private void FindItemByName()
        {
            Console.WriteLine("Item name ?");
            var itemName = Console.ReadLine();

            var item = GetGoodsByName(itemName);

            if (item == null)
            {
                Console.WriteLine("Item not found");
            }
            else
            {
                Console.WriteLine($"{item.Name}, {item.Category}, {item.Description}");
            }
        }

        private void CreateAccount()
        {
            Console.WriteLine("email ?");
            var email = Console.ReadLine();

            Console.WriteLine("password ?");
            var pass = Console.ReadLine();

            Console.WriteLine("name ?");
            var name = Console.ReadLine();

            Console.WriteLine("role ? (1 - registed, 2 - admin)");
            var role = Console.ReadLine();

            CreateAccount(email, pass, name, role == "2" ? UserRoles.Admin : UserRoles.Registered);
        }
    }
}
