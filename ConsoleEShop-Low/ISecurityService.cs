﻿namespace ConsoleEShop_Low
{
    public interface ISecurityService
    {
        User CurrentUser { get; }

        bool Login(string login, string password);

        bool CreateUser(string email, string password, string name, UserRoles role);

        void Logout();
    }
}
