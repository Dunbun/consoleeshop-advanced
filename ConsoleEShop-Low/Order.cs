﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Low
{
    public class Order: ValueObject
    {
        public int Id { get; set; }
        public OrderStatus Status { get; set; }
        public string GoodsName { get; set; }
        public int UserId { get; set; }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Id;
            yield return Status;
            yield return GoodsName;
            yield return UserId;

        }
    }
}
